import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {useEffect} from 'react';
import HomeComponent from '../components/section/HomeComponent';
import {useDispatch, useSelector} from 'react-redux';
import firestore from '@react-native-firebase/firestore';

const Home = ({navigation}) => {
  const dispatch = useDispatch();
  const {dataList} = useSelector(state => state.list);
  console.log('data', dataList);
  const onDelete = id => {
    dispatch({type: 'DELETE', id: id});
  };

  useEffect(() => {
    getData();
  }, []);

  const getData = async () => {
    console.log('masuk sini');
    const user = await firestore().collection('Note').doc('note').get();
    console.log('masuk firebase');
    console.log('notefirebase', user);
  };

  return (
    <View style={{flex: 1, padding: 12}}>
      <HomeComponent
        navigation={navigation}
        data={dataList}
        onDelete={onDelete}
      />
    </View>
  );
};

export default Home;

const styles = StyleSheet.create({});
