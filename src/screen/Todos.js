import React, {useEffect, useState} from 'react';
import {FlatList, ScrollView, Text, View} from 'react-native';

import firestore from '@react-native-firebase/firestore';
import {Appbar, TextInput} from 'react-native';
import {CheckBox, Button} from 'react-native-elements';

function Todos() {
  const [todo, setTodo] = useState('');
  const [done, setDone] = useState(false);
  const [loading, setLoading] = useState(true);
  const [todos, setTodos] = useState([]);
  const ref = firestore().collection('todos');

  useEffect(() => {
    getData();
  }, []);

  const getData = () => {
    ref.onSnapshot(querySnapshot => {
      const list = [];
      querySnapshot.forEach(doc => {
        const {title, complete} = doc.data();
        list.push({
          id: doc.id,
          title,
          complete,
        });
      });
      setTodos(list);
      if (loading) {
        setLoading(false);
      }
    });
  };

  async function addTodo() {
    await ref.add({
      title: todo,
      complete: false,
    });
    setTodo('');
  }

  async function UpdateComplete(value) {
    await ref.doc(value.id).update({
      title: value.title,
      complete: !value.complete,
    });
  }

  async function deleteList(value) {
    await ref.doc(value.id).delete();
  }

  return (
    <View style={{flex: 1, backgroundColor: 'white'}}>
      <ScrollView>
        <FlatList
          data={todos}
          keyExtractor={item => item.id}
          renderItem={({item}) => (
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                // justifyContent: 'space-between',
              }}>
              <CheckBox
                checked={item.complete}
                size={20}
                onPress={() => UpdateComplete(item)}
              />
              <Text style={{fontSize: 16}}>{item.title}</Text>
              <View style={{right: 10, position: 'absolute'}}>
                <Button
                  color="error"
                  title={'DEL'}
                  onPress={() => deleteList(item)}></Button>
              </View>
            </View>
          )}
        />
      </ScrollView>

      <TextInput
        placeholder={'New Todo'}
        value={todo}
        onChangeText={text => setTodo(text)}
        style={{borderColor: 'grey', borderWidth: 1}}
      />
      <Button onPress={() => addTodo()} size="md" title={'Add TODO'}></Button>
    </View>
  );
}

export default Todos;
